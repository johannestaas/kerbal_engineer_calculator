#!/usr/bin/env python
import json

NAME = 'name'
MOUNT = 'mount'
MASS = 'mass'
THRUST = 'thrust'
ATM_TS = 'atm t/s'
VAC_TS = 'vac t/s'
ATM_ISP = 'atm isp'
VAC_ISP = 'vac isp'
TVC = 'tvc'

DRY_MASS = 'dry mass'
LIQ_FUEL = 'liquid fuel'
OXI_FUEL = 'oxidizer'
FUEL_MASS = 'fuel mass'

FUEL_TYPE = 'fuel'
LIQ_TYPE = 'liquid'
XEN_TYPE = 'xenon'
JET_TYPE = 'jet'

with open('parts.json') as f:
    JSON_PARTS = json.load(f)

class Part(object):

    def __repr__(self):
        return self.name

    def decode(self, dct):
        self.name = dct[NAME]
        self.mass = dct[MASS]

class Engine(Part):

    def __repr__(self):
        return '%s [%.1f t] (%d kN, %d/%d ISP)' % (
            self.name, self.mass, self.thrust, self.atm_isp, self.vac_isp)

    def decode(self, dct):
        Part.decode(self, dct)
        self.mount = dct[MOUNT]
        self.thrust = dct[THRUST]
        self.atm_isp = dct[ATM_ISP]
        self.vac_isp = dct[VAC_ISP]
        self.fuel_type = dct[FUEL_TYPE]

class Tank(Part):

    def __repr__(self):
        return '%s [%.1f t] (%d)' % (
            self.name, self.mass, self.liq_fuel)

    def decode(self, dct):
        Part.decode(self, dct)
        self.mount = dct[MOUNT]
        self.dry_mass = dct[DRY_MASS]
        self.liq_fuel = dct[LIQ_FUEL]
        self.oxi_fuel = dct[OXI_FUEL]
        self.fuel_mass = dct[FUEL_MASS]
        self.fuel_type = dct[FUEL_TYPE]

ALL_PARTS = {}

ENGINES = {}
for engine, data in JSON_PARTS['engines'].items():
    ENGINES[engine] = Engine()
    ENGINES[engine].decode(data)
    ALL_PARTS[engine] = ENGINES[engine]

TANKS = {}
for tank, data in JSON_PARTS['tanks'].items():
    TANKS[tank] = Tank()
    TANKS[tank].decode(data)
    ALL_PARTS[tank] = TANKS[tank]

def get_engine(engine_name):
    return ENGINES[engine_name]

def get_tank(tank_name):
    return TANKS[tank_name]

