#!/usr/bin/env python
from planets import get_planet
from collections import namedtuple
from contextlib import contextmanager

# 1.7 TWR optimal for launching from atmosphere:
# http://forum.kerbalspaceprogram.com/threads/63501-What-is-the-optimal-Thrust-to-Weight-ratio-for-launching-space-travel-and-landing

PathNode = namedtuple('PathNode', 'dvnode cost twr ref')
StageNode = namedtuple('StageNode', 'pathnode orig dest atmo')


class DVNode(object):

    _DIRECT = {}
    KERBOL_TWR = 0.0001
    ORBIT_TWR = 0.3
    ASCENT_TWR = 1.7
    DESCENT_TWR = 2.0

    def __repr__(self):
        return 'dv_%s' % self.name

    def __init__(self, name, planet=None, orbit=None, **neighbors_kwargs):
        self.name = name
        self.neighbors = {}
        self.planet = planet
        self.orbit = orbit
        for node_name, cost in neighbors_kwargs.items():
            node = DVNode.get(node_name)
            if isinstance(cost, tuple):
                self.neighbors[node] = cost[0]
                node.add(cost[1], self)
            else:
                self.neighbors[node] = cost
                node.add(cost, self)
        DVNode._DIRECT[name] = self

    def add(self, cost, node):
        self.neighbors[node] = cost

    @classmethod
    def get(cls, name):
        return cls._DIRECT[name]

    def brute_paths(self, target, chutes=True):
        paths = []
        self._brute_paths(target, [], paths, chutes=chutes)
        paths.sort()
        return paths

    def _brute_paths(self, target, path, paths, chutes=True):
        if self is target:
            # node, cost, min TWR
            path += [PathNode(self, 0, 0.0, None)]
            costs = [x.cost for x in path]
            paths += [(sum(costs), path)]
            return
        dvnodes = [x.dvnode for x in path]
        for node, ncost in self.neighbors.items():
            if node in dvnodes:
                continue
            cost = ncost
            if (
                cost == 0 and not (
                    chutes and
                    target.planet and
                    target.planet.atmo)):
                cost = node.neighbors[self]
            if self.planet:
                min_twr = DVNode.ASCENT_TWR
                ref = self
            elif node.planet and chutes and node.planet.atmo:
                min_twr = DVNode.KERBOL_TWR
                ref = node
            elif node.planet:
                min_twr = DVNode.DESCENT_TWR
                ref = node
            elif node.orbit == dv_kerbol or self.orbit == dv_kerbol:
                min_twr = DVNode.KERBOL_TWR
                ref = self.orbit
            else:
                min_twr = DVNode.ORBIT_TWR
                ref = node.orbit
            path2 = path[:] + [PathNode(self, cost, min_twr, ref)]
            node._brute_paths(target, path2, paths, chutes=chutes)


@contextmanager
def orbit(planet):

    def orbit_node(src, **kwargs):
        return DVNode(src, orbit=planet, **kwargs)

    yield orbit_node


dv_kerbol = DVNode('kerbol', planet=get_planet('kerbol'))
dv_kerbin = DVNode('kerbin', planet=get_planet('kerbin'))
dv_mun = DVNode('mun', planet=get_planet('mun'))
dv_minmus = DVNode('minmus', planet=get_planet('minmus'))
dv_moho = DVNode('moho', planet=get_planet('moho'))
dv_eve = DVNode('eve', planet=get_planet('eve'))
dv_gilly = DVNode('gilly', planet=get_planet('gilly'))
dv_duna = DVNode('duna', planet=get_planet('duna'))
dv_ike = DVNode('ike', planet=get_planet('ike'))
dv_dres = DVNode('dres', planet=get_planet('dres'))
dv_jool = DVNode('jool', planet=get_planet('jool'))
dv_laythe = DVNode('laythe', planet=get_planet('laythe'))
dv_vall = DVNode('vall', planet=get_planet('vall'))
dv_tylo = DVNode('tylo', planet=get_planet('tylo'))
dv_bop = DVNode('bop', planet=get_planet('bop'))
dv_pol = DVNode('pol', planet=get_planet('pol'))
dv_eeloo = DVNode('eeloo', planet=get_planet('eeloo'))

with orbit(dv_kerbin) as KerbinNode:
    dv_kerbin_lo = KerbinNode('kerbin_lo', kerbin=(0, 3200))
    dv_kerbin_gso = KerbinNode('kerbin_gso', kerbin_lo=(0, 1115))
    dv_kerbin_soi = KerbinNode('kerbin_soi', kerbin_lo=(0, 950))
    dv_mun_int = KerbinNode('mun_int', kerbin_lo=(0, 860))
    dv_minmus_int = KerbinNode('minmus_int', kerbin_lo=(0, 930))

dv_mun_lo = DVNode('mun_lo', orbit=dv_mun, mun_int=310, mun=580)

dv_minmus_lo = DVNode('minmus_lo', orbit=dv_minmus, minmus_int=160, minmus=180)

dv_moho_int = DVNode('moho_int', orbit=dv_kerbol, kerbin_soi=760)
dv_moho_lo = DVNode('moho_lo', orbit=dv_moho, moho_int=2410, moho=870)

dv_eve_int = DVNode('eve_int', orbit=dv_kerbol, kerbin_soi=90)
with orbit(dv_eve) as EveNode:
    dv_eve_eo = EveNode('eve_eo', eve_int=(80, 0))
    dv_eve_o = EveNode('eve_lo', eve_eo=(1330, 0), eve=(0, 6000))
    dv_gilly_int = EveNode('gilly_int', eve_eo=60)

dv_gilly_lo = DVNode('gilly_lo', orbit=dv_gilly, gilly_int=410, gilly=30)

dv_duna_int = DVNode('duna_int', orbit=dv_kerbol, kerbin_soi=130)
with orbit(dv_duna) as DunaNode:
    dv_duna_eo = DunaNode('duna_eo', duna_int=(250, 0))
    dv_duna_lo = DunaNode('duna_lo', duna_eo=(360, 0), duna=(0, 1200))
    dv_ike_int = DunaNode('ike_int', duna_eo=30)

dv_ike_lo = DVNode('ike_lo', orbit=dv_ike, ike_int=180, ike=390)

dv_dres_int = DVNode('dres_int', orbit=dv_kerbol, kerbin_soi=1010)
dv_dres_lo = DVNode('dres_lo', orbit=dv_dres, dres_int=1290, dres=430)

dv_jool_int = DVNode('jool_int', orbit=dv_kerbol, kerbin_soi=980)
with orbit(dv_jool) as JoolNode:
    dv_jool_eo = JoolNode('jool_eo', jool_int=160)
    dv_jool_lo = JoolNode('jool_lo', jool_int=(2810, 0), jool=(0, 10000))
    dv_pol_int = JoolNode('pol_int', jool_eo=160)
    dv_bop_int = JoolNode('bop_int', jool_eo=220)
    dv_tylo_int = JoolNode('tylo_int', jool_eo=400)
    dv_vall_int = JoolNode('vall_int', jool_eo=620)
    dv_laythe_int = JoolNode('laythe_int', jool_eo=930)

dv_pol_lo = DVNode('pol_lo', orbit=dv_pol, pol_int=820, pol=130)
dv_bop_lo = DVNode('bop_lo', orbit=dv_bop, bop_int=900, bop=220)
dv_tylo_lo = DVNode('tylo_lo', orbit=dv_tylo, tylo_int=1100, tylo=2270)
dv_vall_lo = DVNode('vall_lo', orbit=dv_vall, vall_int=910, vall=860)
dv_laythe_lo = DVNode(
    'laythe_lo', orbit=dv_laythe, laythe_int=(1070, 0), laythe=(0, 2600)
)

dv_eeloo_int = DVNode('eeloo_int', orbit=dv_kerbol, kerbin_soi=1140)
dv_eeloo_lo = DVNode('eeloo_lo', orbit=dv_eeloo, eeloo_int=1370, eeloo=620)


def flatten_stages(
    orig_name, dest_name, chutes=False, compress=False, round_trip=False,
):
    ''' Used to determine how many different stages of the flight there are.
    '''
    stages = []
    orig = DVNode.get(orig_name)
    dest = DVNode.get(dest_name)
    path = orig.brute_paths(dest, chutes=chutes)[0][1]
    start_name = ''
    last_twr = None
    last_ref = None
    last_atmo = False
    atmo = False
    accum_cost = 0
    for i, node1 in enumerate(path[:-1]):
        node2 = path[i+1]
        cost = node1.cost
        twr = node1.twr
        if node1.dvnode.planet is not None:
            atmo = node1.dvnode.planet.atmo
        else:
            atmo = False
        if node2.ref == dv_kerbol:
            ref = dv_kerbol
        else:
            ref = node1.ref
        if last_twr == twr and ref == last_ref:
            accum_cost += cost
        elif compress and not atmo and (
            i > 0 and
            last_twr <= DVNode.ORBIT_TWR and
            twr <= DVNode.ORBIT_TWR
        ):
            twr1 = last_twr * last_ref.planet.grav
            twr2 = twr * ref.planet.grav
            twr, planet_node = max(
                (twr1, last_ref),
                (twr2, ref))
            twr /= planet_node.planet.grav
            accum_cost += cost
            ref = planet_node.planet
        else:
            if accum_cost != 0:
                pnode = PathNode(None, accum_cost, last_twr, last_ref)
                stages += [StageNode(
                    pnode,
                    start_name,
                    node1.dvnode.name,
                    last_atmo
                )]
            accum_cost = cost
            start_name = node1.dvnode.name
            last_twr = twr
            last_ref = ref
            last_atmo = atmo
    if accum_cost != 0:
        pnode = PathNode(None, accum_cost, last_twr, last_ref)
        stages += [StageNode(
            pnode,
            start_name,
            node2.dvnode.name,
            node2.dvnode.planet is not None and node2.dvnode.planet.atmo,
        )]
    if round_trip:
        ret_stages = flatten_stages(dest_name, orig_name, chutes=chutes,
                                    compress=compress)
        stages += ret_stages
    return stages


def output_stages(stages):
    ''' Spit out stages for debugging '''
    # ref = None
    total_dv = 0
    print('%10s\t%4s\t%10s\t%10s\t%10s\t%10s' % (
        'Delta-V', 'TWR', 'Reference',
        'Start', 'End', 'Atmosphere'))
    for pnode, orig, dest, atmo in stages:
        print('%10s\t%2.2f\t%10s\t%10s\t%10s\t%10s' % (
            pnode.cost, pnode.twr,
            pnode.ref.name, orig, dest,
            str(atmo),
        ))
        total_dv += pnode.cost
    print('='*10)
    print('%10s' % total_dv)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('origin')
    parser.add_argument('destination')
    parser.add_argument('--parachutes', '-p', action='store_true')
    parser.add_argument('--compress', '-c', action='store_true')
    parser.add_argument('--round-trip', '-r', action='store_true')
    args = parser.parse_args()
    stages = flatten_stages(
        args.origin, args.destination, chutes=args.parachutes,
        compress=args.compress, round_trip=args.round_trip,
    )
    output_stages(stages)
