#!/usr/bin/env python

import sys
import math
from collections import namedtuple
from itertools import chain

from parts import ALL_PARTS, ENGINES, TANKS, get_engine, get_tank
from planets import PLANETS, get_planet, get_moon
from deltav_graph import DVNode, StageNode, PathNode

Combo = namedtuple('Combo', 'fitness dv twr isp ship_mass secs engines tanks asparagus')

# For full throttle.
XENON_ELEC_PER_SEC = 14.442
# Using the lighter 1x6,2x3 panels. It's 150 for the static ones.
ELEC_PER_TON = 114.2857143
# Added xenon-electricity requirement mass, per PB-ION
XENON_ELEC_MASS = XENON_ELEC_PER_SEC / ELEC_PER_TON
# TT-38K, Radial decouplers for asparagus staging
RAD_DECOUPLER_MASS = 0.025

def total_thrust(engines):
    ''' Thrust of list of engines '''
    return sum([x.thrust for x in engines])

def total_mass(all_parts):
    ''' Mass of list of parts '''
    return sum([x.mass for x in all_parts])

def total_twr(mass, engines, planet):
    ''' calculate total twr given mass and a set of engines '''
    return calc_twr(
        total_mass(engines) + mass,
        total_thrust(engines),
        planet.grav
    )

def total_seconds(engines, tanks, atmo=False):
    ''' Calculate seconds of fuel '''
    isp = total_isp(engines, atmo=atmo)
    if isp == 0:
        return 0
    consumption_rate = total_thrust(engines) * 1000.0 / (isp * 9.82)
    consumption_rate /= 5000.0
    fuel = sum([x.fuel_mass for x in tanks])
    return fuel / consumption_rate / 5.0

def total_isp(engines, atmo=False):
    ''' total isp of engines '''
    if atmo:
        isp_attr = 'atm_isp'
    else:
        isp_attr = 'vac_isp'
    sum_thrust = 0
    sum_thrust_isp = 0
    for engine in engines:
        if getattr(engine, isp_attr) == 0:
            continue
        sum_thrust += engine.thrust
        sum_thrust_isp += engine.thrust / getattr(engine, isp_attr)
    if sum_thrust_isp == 0:
        return 0
    return sum_thrust / sum_thrust_isp

def total_dv(mass, engines, tanks, atmo=False):
    ''' calculate deltav given a set of parts '''
    mass_start = mass + total_mass(engines + tanks)
    mass_dry = sum([x.dry_mass for x in tanks])
    mass_end = mass + total_mass(engines) + mass_dry
    isp = total_isp(engines, atmo=atmo)
    return math.log(mass_start / mass_end, math.e) * isp * 9.81

def calc_asparagus(mass, engine_tank_pairs, planet, atmo=False):
    ''' calculate deltav of asparagus staged pairs.
    The first index will be dropped, then the next, and so on.
    '''
    dv = 0
    twrs = []
    for i, engines_tanks in enumerate(engine_tank_pairs):
        engines, tanks = engines_tanks
        all_engines = []
        decoupler_mass = 0.0
        for stage_engines, stage_tanks in engine_tank_pairs[i:]:
            all_engines += stage_engines
            decoupler_mass += len(stage_tanks) * RAD_DECOUPLER_MASS
        dv += total_dv(mass + decoupler_mass, all_engines, tanks, atmo=atmo)
        twrs += [total_twr(mass + decoupler_mass, all_engines, planet)]
    return dv, twrs

def calc_twr(mass, thrust, gravity):
    ''' calculate twr given a few raw floats '''
    return thrust / (mass * gravity)

def get_names(all_parts):
    ''' names of all the parts '''
    names = ''
    name_d = {}
    for part in all_parts:
        if name_d.get(part, 0) == 0:
            name_d[part] = 1
        else:
            name_d[part] += 1
    for part, count in name_d.items():
        names += '%d x %s, ' % (count, part.name)
    return names[:-2]

def print_combo(combo):
    if combo.asparagus:
        print('Staging: Asparagus')
    else:
        print('Staging: Standard')
    print('Fitness: %f' % combo.fitness)
    print('DeltaV : %f' % combo.dv)
    print('TWR    : %f' % combo.twr)
    if not combo.asparagus:
        print('Seconds: %f' % combo.secs)
    print('Mass   : %f' % combo.ship_mass)
    if combo.asparagus:
        print('%d stages of' % len(combo.engines))
        print('\tEngines: %s' % get_names(combo.engines[0]))
        print('\tTanks  : %s\n' % get_names(combo.tanks[1]))
    else:
        print('Engines: %s' % get_names(combo.engines))
        print('Tanks  : %s\n' % get_names(combo.tanks))

def proper_num_engines(tank, engine, tanks_n):
    if tank.fuel_type != engine.fuel_type:
        return []
    if tank.mount == 'Radial mounted':
        return []
    if tank.mount == 'Tiny':
        valid = {
            'Radial mounted': {
                1: [2],
                2: [2, 4],
                4: [4, 8],
                8: [8, 16],
                16: [16],
            },
            'Tiny': {
                1: [1, 2],
                2: [2, 4],
                4: [4, 8],
                8: [8, 16],
                16: [16, 32],
            },
        }
    elif tank.mount == 'Small':
        valid = {
            'Radial mounted': {
                1: [2, 4, 8],
                2: [2, 4, 8],
                4: [4, 8],
                8: [8],
                16: [16],
            },
            'Tiny': {
                1: [1, 2, 3, 4],
                2: [2, 4, 6, 8],
                4: [4, 8, 12, 16],
                8: [8, 16, 24],
                16: [16, 32],
            },
            'Small': {
                1: [1],
                2: [2],
                4: [4],
                8: [8],
                16: [16],
            },
        }
    elif tank.mount == 'Large':
        valid = {
            'Radial mounted': {
                1: [2, 4, 8],
                2: [2, 4, 8],
                4: [4, 8],
                8: [8, 16],
                16: [16, 32],
            },
            'Tiny': {
                1: [1, 2, 3, 4, 6, 8],
                2: [2, 4, 6, 8, 12, 16],
                4: [4, 8, 12, 16, 24, 32],
                8: [8, 16, 24, 32, 48, 64],
                16: [16, 32, 48, 64],
            },
            'Small': {
                1: [1, 2, 3, 4],
                2: [2, 4, 6, 8],
                4: [4, 8, 12, 16],
                8: [8, 16, 24, 32],
                16: [16, 32, 48, 64]
            },
            'Large': {
                1: [1],
                2: [2],
                4: [4],
                8: [8],
                16: [16],
            },
        }
    else:
        raise ValueError('Mount type: %s' % tank.mount)
    if engine.mount not in valid:
        return []
    if tanks_n not in valid[engine.mount]:
        return []
    return valid[engine.mount][tanks_n]

def tank_engine_combo():
    for tank_name, tank in TANKS.items():
        for tanks_n in [1,2,4,8,16]:
            tanks = [tank] * tanks_n
            for engine_name, engine in ENGINES.items():
                for n in proper_num_engines(tank, engine, tanks_n):
                    engines = [engine] * n
                    yield engines, tanks

def asparagus_combo():
    for stages in [2,3,4,5]:
        for tank_name, tank in TANKS.items():
            for tanks_n in [2,4]:
                tanks = [tank] * tanks_n
                for engine_name, engine in ENGINES.items():
                    for n in proper_num_engines(tank, engine, tanks_n):
                        engines = [engine] * n
                        pairs = []
                        for stage in range(stages):
                            pairs += [(engines, tanks)]
                        yield pairs


def target_asparagus(payload_mass, stages):
    pass


def target(payload_mass, stage, asp=False):
    ''' Find the best tank/engine combo to get a good deltav and TWR '''
    combos = []
    atmo = stage.atmo
    planet = stage.pathnode.ref.planet
    asparagus = asp or (
        stage.pathnode.twr == DVNode.ASCENT_TWR and atmo)
    for engines, tanks in tank_engine_combo():
        mass = payload_mass
        mass += XENON_ELEC_MASS * engines.count(get_engine('IX-6315 Dawn'))
        twr = total_twr(mass + total_mass(tanks), engines, planet)
        if twr < stage.pathnode.twr:
            continue
        secs = total_seconds(engines, tanks, atmo=atmo)
        isp = total_isp(engines, atmo=atmo)
        dv = total_dv(mass, engines, tanks, atmo=atmo)
        if dv < stage.pathnode.cost:
            continue
        ship_mass = total_mass(engines + tanks) + mass
        fit = ship_mass
        combos += [
            Combo(
                fit, dv, twr, isp, ship_mass, secs,
                engines, tanks, False
            )
        ]
    for engines_tanks_pairs in asparagus_combo():
        if not asparagus:
            break
        engine_sets, tank_sets = zip(*engines_tanks_pairs)
        engines = list(chain(*engine_sets))
        mass = payload_mass
        mass += XENON_ELEC_MASS * engines.count(get_engine('IX-6315 Dawn'))
        tanks = list(chain(*tank_sets))
        secs = -1
        dv, twr_range = calc_asparagus(mass, engines_tanks_pairs, planet,
            atmo=atmo)
        if dv < stage.pathnode.cost:
            continue
        if min(twr_range) < stage.pathnode.twr:
            continue
        ship_mass = (
            total_mass(engines + tanks) +
            mass + len(tanks) * RAD_DECOUPLER_MASS
        )
        fit = ship_mass
        combos += [
            Combo(
                fit, dv, min(twr_range), isp, ship_mass, secs,
                engine_sets, tank_sets, True
            )
        ]
    combos = sorted(combos, key=lambda x: (x[0], x[1], x[2], x[3], x[4], x[5]))
    return combos

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('mass', type=float)
    parser.add_argument('deltav', type=int)
    parser.add_argument('min_twr', type=float)
    parser.add_argument('location')
    parser.add_argument('--asparagus', '-a', action='store_true')
    parser.add_argument('--length', '-l', type=int, default=1)
    parser.add_argument('--start', '-s', type=int, default=0)
    args = parser.parse_args()
    dvnode = DVNode.get(args.location)
    pathnode = PathNode(dvnode, args.deltav, args.min_twr, dvnode.orbit or dvnode)
    stagenode = StageNode(pathnode, None, None, dvnode.planet is not None and dvnode.planet.atmo)
    combos = target(args.mass, stagenode, asp=args.asparagus)
    args.length = args.length if args.length != 0 else len(combos)
    for i in range(args.length):
        try:
            print_combo(combos[i+args.start])
        except IndexError:
            break
